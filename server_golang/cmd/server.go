package cmd

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"profile/config"
	profilefx "profile/di/productfx"
	"profile/di/prometheusfx"
	profile_http "profile/service/profile/router"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	"go.uber.org/fx"
)

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		NewServer().Run()
	},
	Version: "1.0.0",
}

type Server struct{}

func NewServer() *Server {
	return &Server{}
}

func (s *Server) Run() {
	app := fx.New(
		fx.Invoke(config.InitConfig),
		// esfx.Module,
		profilefx.Module,
		// tracingfx.Mowdule,
		prometheusfx.Module,
		fx.Provide(provideGinEngine),
		fx.Invoke(
			registerService,
		),
		fx.Invoke(StartServer),
	)
	app.Run()
}

func provideGinEngine() *gin.Engine {
	return gin.New()
}

func registerService(
	g *gin.Engine,
	productRouter profile_http.ProfileRouter,
	prometheusMdw prometheusfx.Middleware,
	// tracer tracing.Tracer,
) {
	api := g.Group("/api/v1")
	api.Use(prometheusMdw.GetMetrics()...)
	// api.Use(tracer.TracingHandler)

	productRouter.Register(api)
}

func StartServer(lifecycle fx.Lifecycle, g *gin.Engine) {
	lifecycle.Append(
		fx.Hook{
			OnStart: func(context.Context) error {
				go registerServer(g)
				go registerKafka()
				return nil
			},
		},
	)
}

func registerServer(g *gin.Engine) {
	fmt.Println("run on port:8000")
	server := http.Server{
		Addr:    ":8000",
		Handler: g,
	}

	if err := server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		log.Fatal("failed to listen and serve from server: %v", err)
	}
}

func registerKafka() {
	// ctx := context.Background()
	// mechanism := plain.Mechanism{
	// 	Username: "NBD7IEDOHEXPYAGM",
	// 	Password: "j47Qm5MIhjFIIk6d5G8hTBWi9uhBdd4VI6Ty0Gln3w0U+9W3al4R7i6Z09WJ2XfY",
	// }

	// dialer := &kafka.Dialer{
	// 	Timeout:       45000,
	// 	DualStack:     true,
	// 	SASLMechanism: mechanism,
	// }

	// conn, err := dialer.Dial("gcp", "pkc-ldvr1.asia-southeast1.gcp.confluent.cloud:9092")
	// fmt.Println("===========conn", conn)
	// fmt.Println("===========err", err)

	// mechanism, err := scram.Mechanism(scram.SHA512, "m59rg760", "m_GjZ-EHeFzSHrdPDeMTukm-ZZOLtlc2")
	// if err != nil {
	// 	panic(err)
	// }

	// dialer := &kafka.Dialer{
	// 	Timeout:       10 * time.Second,
	// 	DualStack:     true,
	// 	SASLMechanism: mechanism,
	// }

	// conn, err := dialer.DialContext(ctx, "tcp", "moped.srvs.cloudkafka.com")
	// fmt.Println("===========conn", conn)
	// fmt.Println("===========err", err)
	// r := kafka.NewReader(kafka.ReaderConfig{

	// 	Brokers: []string{"pkc-ldvr1.asia-southeast1.gcp.confluent.cloud:9092"},
	// 	Topic:   "Topic1",
	// 	Dialer:  dialer,
	// })
	// r := kafka.NewReader(kafka.ReaderConfig{
	// 	Brokers: []string{"localhost:9092"},
	// 	GroupID: "consumer-group-id",
	// 	Topic:   "Topic2",
	// 	// Partition: 1,
	// 	MinBytes: 10e3, // 10KB
	// 	MaxBytes: 10e6, // 10MB
	// })

	// for {
	// 	m, err := r.FetchMessage(ctx) // FetchMessage does not commit offsets automatically when using consumer groups. Use CommitMessages to commit the offset.
	// 	// m, err := r.ReadMessage(context.Background()) // FetchMessage does not commit offsets automatically when using consumer groups. Use CommitMessages to commit the offset.
	// 	if err != nil {
	// 		fmt.Println("err: ", err)
	// 		break
	// 	}
	// 	fmt.Printf("message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))
	// }

	// if err := r.Close(); err != nil {
	// 	log.Fatal("failed to close reader:", err)
	// }
}
