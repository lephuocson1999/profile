package main

import (
	"fmt"
	"profile/cmd"
	"strings"
)

func main() {
	check1 := isPalindrome(121)
	check2 := isPalindrome(123)
	fmt.Println("===========check1", check1)
	fmt.Println("===========check2", check2)
	cmd.Execute()
}

func isPalindrome(x int) bool {
	parseX := fmt.Sprintf("%v", x)
	splitX := strings.Split(parseX, "")
	fmt.Println("splitX", splitX)

	arrString := make([]string, len(splitX))
	for i := range splitX {
		position := len(splitX) - i - 1
		arrString[position] = splitX[i]
	}

	result := strings.Join(arrString, "")
	return result == parseX
}
