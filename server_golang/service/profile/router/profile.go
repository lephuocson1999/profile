package router

import (
	"github.com/gin-gonic/gin"
)

type ProfileRouter interface {
	Register(routerGroup gin.IRouter)
}

type profileRouterIml struct {
}

func NewProductRouter() ProfileRouter {
	return &profileRouterIml{}
}
func (p *profileRouterIml) Register(r gin.IRouter) {
	product := r.Group("/profile")
	{
		product.GET("/detail", p.GetProfileDetail)
	}
}

func (p *profileRouterIml) GetProfileDetail(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "done",
		"data":    "done",
	})
}
