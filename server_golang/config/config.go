package config

import (
	"log"

	"github.com/kelseyhightower/envconfig"
)

var (
	server       ServerCfg
	dbCfg        DBCfg
	grpcCfg      GrpcServiceCfg
	jaegerConfig JaegerConfig
)

type DBCfg struct {
	MongoURI string `envconfig:"MONGO_URI" default:"0.0.0.0"`
}

type ServerCfg struct {
	SERVERUrl string `envconfig:"SERVER_URL" default:"0.0.0.0"`
	ESDomain  string `envconfig:"ES_DOMAIN" default:"http://localhost:49154"`
}

type GrpcServiceCfg struct {
	GrpcEndpoint string `envconfig:"GRPC_ENDPOINT_SERVICE" default:"localhost:10010"`
}

type JaegerConfig struct {
	Enabled      bool    `envconfig:"JAEGER_ENABLED" default:"true"`
	ServiceName  string  `envconfig:"JAEGER_SERVICE_NAME" default:"profile"`
	Endpoint     string  `envconfig:"JAEGER_ENDPOINT" default:"127.0.0.1:6382"`
	SamplerType  string  `envconfig:"JAEGER_SAMPLER_TYPE" default:"const"`
	SamplerParam float64 `envconfig:"JAEGER_SAMPLER_PARAM" default:"1"`
}

func InitConfig() {
	configs := []interface{}{
		&server,
		&dbCfg,
		&grpcCfg,
		&jaegerConfig,
	}
	for _, instance := range configs {
		err := envconfig.Process("", instance)
		if err != nil {
			log.Fatalf("unable to init config: %v, err: %v", instance, err)
		}
	}
}

func ServerConfig() ServerCfg {
	return server
}

func TracingConfig() JaegerConfig {
	return jaegerConfig
}
