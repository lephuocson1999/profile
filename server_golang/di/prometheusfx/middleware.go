package prometheusfx

import (
	"github.com/gin-gonic/gin"
)

type Middleware interface {
	GetMetrics() []gin.HandlerFunc
}

type middleware struct {
	serviceName string
	collector   Collector
}

func NewMiddleware(collector Collector) Middleware {
	m := &middleware{
		collector: collector,
	}
	m.collectorLatency()
	m.collectorLatencyHistogram()
	// m.collectorRequestSize()
	// m.collectorRequestTotal()
	return m
}

func (m *middleware) GetMetrics() []gin.HandlerFunc {
	return []gin.HandlerFunc{
		m.Latency,
		m.LatencyHistogram,
		// m.RequestSize,
		// m.ResponseStatus,
	}
}
