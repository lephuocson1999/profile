package profilefx

import (
	profile_http "profile/service/profile/router"

	"go.uber.org/fx"
)

var Module = fx.Provide(provideProductRouter)

func provideProductRouter() profile_http.ProfileRouter {
	return profile_http.NewProductRouter()
}
