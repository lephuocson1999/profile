package esfx

import (
	"context"
	"fmt"
	"profile/config"
	"time"

	"github.com/olivere/elastic/v7"
	"go.uber.org/fx"
)

const defaultTimeout = 10 * time.Second

var Module = fx.Provide(
	provideESClient,
)

func provideESClient(lifecycle fx.Lifecycle) (*elastic.Client, error) {
	es, err := elastic.NewClient(elastic.SetURL(config.ServerConfig().ESDomain))
	if err != nil {
		return nil, err
	}

	_, err = es.IndexExists("my-index-01").Do(context.Background())
	if err != nil {
		// Handle error
	}
	fmt.Println("ES: run on port: ", config.ServerConfig().ESDomain)
	return es, nil
}
